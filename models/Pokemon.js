import mongoose from "mongoose";

const Pokemon = mongoose.model("Pokemon", new mongoose.Schema({
    name: String,
    shiny: Boolean,
    quantity: {
        type: Number,
        default: 1,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    UpdatedAt: {
        type: Date,
        default: Date.now,
    },
}));

export default Pokemon;