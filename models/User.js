import mongoose from "mongoose";

const User = mongoose.model("User", new mongoose.Schema({
    discordID: String,
    role: {
        type: String,
        default: "user",
    }
}));

export default User;