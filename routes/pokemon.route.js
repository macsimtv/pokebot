import pokemonController from "../controllers/pokemon.controller.js";

const prefix = process.env.PREFIX;

const routePokemon = (client, msg, website_url = '') => {
    switch(msg.content.split(' ')[1]) {
        case undefined:
            pokemonController.getRandomPokemon(client, msg);
            break;
        case `inventory`:
            pokemonController.getInventory(client, msg, website_url);
            break;
        case `shinies`:
            pokemonController.getShinies(client, msg);
            break;
        default:
            break;
    }
}  

export default routePokemon;