import dotenv from "dotenv";
import Discord from "discord.js";
import mongoose from "mongoose";

dotenv.config();

// ROUTES
import routePokemon from "./routes/pokemon.route.js";

// DB
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.on("connected", () => console.log("Connected to MongoDB"));

// DISCORD
const client = new Discord.Client({
  intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES],
});
const prefix = process.env.PREFIX;
const website_url = process.env.WEBSITE_LINK;

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);

  client.user.setPresence({
    activities: [{
        name: `${prefix}help | ${client.guilds.cache.size} servers! 🍻`,
    }]
  });
});

const findTerm = (word, term) => {
  if (word.includes(term)) {
    return word;
  }
};

client.on("message", (msg) => {
  switch (msg.content) {
    case findTerm(msg.content, `${prefix}pokemon`):
      routePokemon(client, msg, website_url);
      break;
    case findTerm(msg.content, `${prefix}invite`):
      msg.channel.send({
        embeds: [
          {
            title: "Invite me in your Discord Server",
            description: "Click on the title to invite me :)",
            url: process.env.INVITE_LINK,
            color: null,
          },
        ],
      });
      break;
    case findTerm(msg.content, `${prefix}help`):
      msg.channel.send({
        embeds: [
          {
            title: "Help",
            description: "Here are the commands you can use:",
            fields: [
              {
                name: `${prefix}help`,
                value: "Show this message",
                inline: false,
              },
              {
                name: `${prefix}invite`,
                value: "Invite me in your Discord Server",
                inline: false,
              },
              {
                name: `${prefix}pokemon`,
                value: "Get a random pokemon",
                inline: false,
              },
              {
                name: `${prefix}pokemon inventory`,
                value: "Get your inventory",
                inline: false,
              },
              {
                name: `${prefix}pokemon shinies`,
                value: "Get your shiny pokemons",
                inline: false,
              },
            ],
            footer: {
              text: `Currently in ${client.guilds.cache.size} servers`,
            },
            color: null,
          },
        ],
      });
    default:
      break;
  }
});

// client.on("guildCreate", (guild) => {
//   client.user.setPresence({
//     status: 'dnd',
//     activities: [{
//         name: `${prefix}help | Currently in ${client.guilds.cache.size} servers`,
//         type: 'LINK',
//         url: 'https://www.twitch.tv/macsimtv'
//     }]
//   });
// });

client.login(process.env.TOKEN);
