import fetch from 'node-fetch';

import User from '../models/User.js';
import Pokemon from '../models/Pokemon.js';

import pokemonNameBeautify from '../lib/pokemonNameBeautify.js';

class PokemonController {
    static async getRandomPokemon(client, msg) {
        let pokemonName;
        let isUserCancel = false;

        try {
            // Check if user is created
            const user = await User.findOne({ discordID: msg.author.id });

            if (!user) {
                // Create user
                const newUser = new User({
                    discordID: msg.author.id,
                });
                await newUser.save();
            } else {
                // Check last time user asked for a pokemon
                const lastPokemon = await Pokemon.findOne({ user: user._id }, {}, { sort: { 'UpdatedAt': -1 } });
                if(lastPokemon) {
                    const lastPokemonDate = new Date(lastPokemon.UpdatedAt);
                    const now = new Date();
                    const diff = now - lastPokemonDate;
                    const diffMinutes = Math.floor((diff / 1000) / 60);

                    if(diffMinutes < 15 && user.role === "user") {
                        isUserCancel = true;
                        throw new Error('You can only ask for a pokemon every 15 minutes');
                    }
                }
            }

            const res = await fetch('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=10000');
            const pokemons = await res.json();
            const randomPokemon = pokemons.results[Math.floor(Math.random() * pokemons.results.length)];

            // Pokemon name
            pokemonName = pokemonNameBeautify(randomPokemon.name);
    
            // Pokemon Shiny
            let isShiny = false;
            const shinyChance = 2;
            if ((Math.random() * 100) < shinyChance) {
                const isAvailableShiny = await fetch(`https://play.pokemonshowdown.com/sprites/xyani-shiny/${randomPokemon.name}.gif`);
                if(isAvailableShiny.status === 200) {
                    isShiny = true;
                }
            } else {
                const isAvailable = await fetch(`https://play.pokemonshowdown.com/sprites/xyani/${randomPokemon.name}.gif`);
                if(isAvailable.status !== 200) {
                    throw new Error('Pokemon not found');
                }
            }
    
            let pokemonDisplayInformation = {
                name: pokemonName,
            }

            let pokemonDatabaseInformation = {
                name: randomPokemon.name,
                shiny: isShiny,
                user: user._id,
            }

            // Check if pokemon is already in database
            const filter = {
                name: randomPokemon.name,
                shiny: isShiny,
                user: user._id
            }

            const pokemon = await Pokemon.findOne(filter);

            if(pokemon) {
                console.log('Pokemon already in database');
                // Pokemon already in database
                await Pokemon.findOneAndUpdate(filter , {
                    quantity: pokemon.quantity + 1,
                    UpdatedAt: Date.now(),
                });
            } else {
                // Save Pokemon to database
                const newPokemon = new Pokemon({
                    ...pokemonDatabaseInformation,
                    quantity: 1,
                    UpdatedAt: Date.now(),
                });

                await newPokemon.save();
            }

            // Everything OK -> Send pokemon
            msg.channel.send({
                embeds: [
                    {
                        "description": `<@${msg.author.id}>, <:pokeball:991726491765317672> you've caught a **${isShiny ? 'Shiny ' + pokemonDisplayInformation.name : pokemonDisplayInformation.name}**`,
                        "color": 2434341,
                        "footer": !pokemon ? {
                            "text": `${pokemonDisplayInformation.name} has been registered to your Pokédex`
                        } : {},
                        "image": {
                            "url": `${isShiny ? `https://play.pokemonshowdown.com/sprites/xyani-shiny/${randomPokemon.name}.gif` : `https://play.pokemonshowdown.com/sprites/xyani/${randomPokemon.name}.gif`}`
                        }
                    }
                ],
            });
        } catch(err) {
            if(isUserCancel) {
                msg.channel.send(`<@${msg.author.id}> you can only ask for a pokemon every 15 minutes`);
            } else {
                if(pokemonName && pokemonName.length > 0) {
                    console.log('Pokemon not found', pokemonName);
                    PokemonController.getRandomPokemon(client, msg);
                } else {
                    console.log('Pokemon not found');
                    PokemonController.getRandomPokemon(client, msg);
                }
            }
        }
        
    }

    static async getInventory(client, msg, website_url = 'http://localhost:3000') {
        try {
            // Check if user is created
            let user = await User.findOne({ discordID: msg.author.id });

            if (!user) {
                // Create user
                user = new User({
                    discordID: msg.author.id,
                });
                await user.save();
            }

            const pokemons = await Pokemon.find({ user: user._id });

            // Send link of website
            msg.channel.send({
                embeds: [
                    {
                        "title": "View your inventory",
                        "description": `<@${msg.author.id}>, <:pokeball:991726491765317672> you have ${pokemons.length} pokemons in your inventory`,
                        "url": `${website_url}/${user.id}/pokemon`,
                        "color": 2434341,
                    }
                ],
            });
        } catch(err) {
            msg.channel.send(`<@${msg.author.id}>, <:pokeball:991726491765317672> something went wrong while trying to get your inventory`);
        }
    }

    static async getShinies(client, msg) {
        try {
            // Check if user is created
            const user = await User.findOne({ discordID: msg.author.id });

            if (!user) {
                // Create user
                const newUser = new User({
                    discordID: msg.author.id,
                });
                await newUser.save();
            }

            // Get pokemon from database
            const pokemons = await Pokemon.find({ user: user._id, shiny: true });

            // Send pokemon
            msg.channel.send({
                embeds: [
                    {
                        "description": `<@${msg.author.id}>, <:pokeball:991726491765317672> you have ${pokemons.length} shiny pokemons in your inventory`,
                        "color": 2434341,
                        "fields": pokemons.map(pokemon => {
                            return {
                                "name": `${pokemonNameBeautify(pokemon.name)}`,
                                "value": `${pokemon.quantity}x`,
                                "quantity": pokemon.quantity
                            }
                        }),
                    }
                ],
            });
        } catch(err) {
            msg.channel.send(`<@${msg.author.id}>, <:pokeball:991726491765317672> something went wrong while trying to get your inventory`);
        }
    }

}

export default PokemonController;
