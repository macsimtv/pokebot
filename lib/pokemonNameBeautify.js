const pokemonNameBeautify = (name) => {
    name = name.replace('-', ' ');
    name = name[0].toUpperCase() + name.slice(1);
    name = name.replace('gmax', 'G-Max');
    name = name.replace('galar', 'Galar');
    name = name.replace('mega', 'Mega');
    name = name.replace('alola', 'Alola');
    return name;
}

export default pokemonNameBeautify;